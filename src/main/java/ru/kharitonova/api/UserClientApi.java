package ru.kharitonova.api;

import io.smallrye.common.annotation.NonBlocking;
import io.smallrye.graphql.client.typesafe.api.GraphQLClientApi;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;

@GraphQLClientApi(configKey = "users-typesafe")
//@GraphQLClientApi(endpoint = "http://localhost:8080/graphql")
public interface UserClientApi {

    public Uni<UserEntity> getUser(@Name("userId") long id);

    public Uni<PassportEntity> getPassport(@Name("passportId") long id);

    public Uni<PassportEntity> getPassportByUser(@Name("userId") long id);

}
