package ru.kharitonova.api.example;

import io.smallrye.graphql.client.typesafe.api.GraphQLClientApi;
import ru.kharitonova.wrapper.example.FilmTypesafeConnection;

@GraphQLClientApi(configKey = "star-wars-typesafe")
public interface StarWarsClientApi {

    FilmTypesafeConnection allFilms();

}
