package ru.kharitonova.entity;

import ru.kharitonova.wrapper.PasswordTypesafeConnection;

import java.time.LocalDateTime;

public class UserEntity {

    private long id;

//    private PasswordTypesafeConnection passwordConnection;

    private LocalDateTime created;

    private String fst_name;

    private String mid_name;

    private String lst_name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getFst_name() {
        return fst_name;
    }

    public void setFst_name(String fst_name) {
        this.fst_name = fst_name;
    }

    public String getMid_name() {
        return mid_name;
    }

    public void setMid_name(String mid_name) {
        this.mid_name = mid_name;
    }

    public String getLst_name() {
        return lst_name;
    }

    public void setLst_name(String lst_name) {
        this.lst_name = lst_name;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", created=" + created +
                ", fst_name='" + fst_name + '\'' +
                ", mid_name='" + mid_name + '\'' +
                ", lst_name='" + lst_name + '\'' +
                '}';
    }

}
