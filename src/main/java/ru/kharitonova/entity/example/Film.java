package ru.kharitonova.entity.example;

import ru.kharitonova.wrapper.example.PlanetTypesafeConnection;

public class Film {

    private String title;

    private PlanetTypesafeConnection planetConnection;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PlanetTypesafeConnection getPlanetConnection() {
        return planetConnection;
    }

    public void setPlanetConnection(PlanetTypesafeConnection planetConnection) {
        this.planetConnection = planetConnection;
    }

    @Override
    public String toString() {
        return "Film {" +
                "title='" + title + '\'' +
                ", planetConnection=" + planetConnection +
                '}';
    }

}
