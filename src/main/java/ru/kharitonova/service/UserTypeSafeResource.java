package ru.kharitonova.service;

import io.smallrye.mutiny.Uni;
import ru.kharitonova.api.UserClientApi;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/")
public class UserTypeSafeResource {

    @Inject
    UserClientApi typesafeClient;

    @GET
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<UserEntity> getUser(long id) {
        return typesafeClient.getUser(id);
    }

    @GET
    @Path("/passport/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<PassportEntity> getPassport(long id) {
        return typesafeClient.getPassport(id);
    }

    @GET
    @Path("passportByUser/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<PassportEntity> getPassportByUser(long id) {
        return typesafeClient.getPassportByUser(id);
    }

}
