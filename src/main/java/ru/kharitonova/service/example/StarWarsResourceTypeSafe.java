package ru.kharitonova.service.example;

import io.smallrye.common.annotation.Blocking;
import ru.kharitonova.api.example.StarWarsClientApi;
import ru.kharitonova.entity.example.Film;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/starwars")
public class StarWarsResourceTypeSafe {
    @Inject
    StarWarsClientApi typesafeClient;

    @GET
    @Path("/typesafe")
    @Produces(MediaType.APPLICATION_JSON)
    @Blocking
    public List<Film> getAllFilmsUsingTypesafeClient() {
        return typesafeClient.allFilms().getFilms();
    }

}