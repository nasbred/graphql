package ru.kharitonova.wrapper;

import io.smallrye.mutiny.Uni;
import ru.kharitonova.entity.UserEntity;

import java.util.List;

public class UserTypesafeConnection {

    private List<UserEntity> users;

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public Uni<UserEntity> getUser(long id) {
        return getUser(id);
    }

}
