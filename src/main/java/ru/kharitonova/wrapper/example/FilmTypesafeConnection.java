package ru.kharitonova.wrapper.example;

import ru.kharitonova.entity.example.Film;

import java.util.List;

public class FilmTypesafeConnection {

    private List<Film> films;

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }
}
