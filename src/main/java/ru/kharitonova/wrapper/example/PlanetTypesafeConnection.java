package ru.kharitonova.wrapper.example;

import ru.kharitonova.entity.example.Planet;

import java.util.List;

public class PlanetTypesafeConnection {

    private List<Planet> planets;

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    @Override
    public String toString() {
        return "PlanetConnection{" +
                "planets=" + planets +
                "}";
    }

}
